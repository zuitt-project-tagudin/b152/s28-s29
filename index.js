
const express = require("express");
const app = express();
const port = 4000;
app.use(express.json());

let users = [

	{
		username: "tinaRCBC",
		email: "tinaLRCBC@gmail.com",
		password: "tinaRCBC990"
	},
	{
		username: "gwenstacy",
		email: "spidergwen@gmail.com",
		password: "gwenOGspider"
	}
];

let items = [

	{
		name: "Stick-O",
		price: 70,
		isActive: true
	},
	{
		name: "Doritos",
		price: 150,
		isActive: true
	}

]


//USERS
app.get('/', (req, res)=> {
	res.send("Hellow world from our first ExpressJS API")
});

app.get('/hello', (req, res)=> {
	res.send("Hello from batch 152")
})

app.get('/users', (req,res) => {
	res.send(users);
})

app.post('/users', (req,res) => {
	console.log(req.body);

	let newUser = {
		username: req.body.username,
		email: req.body.email,
		password: req.body.password
	}

	users.push(newUser);
	console.log(users);

	res.send(users);

})

app.delete('/users', (req,res) => {
	users.pop();
	console.log(users);
	res.send(users);
})

app.put('/users/:index', (req, res) => {
	console.log(req.body);
	console.log(req.params);

	let index = parseInt(req.params.index);

	users[index].password = req.body.password;

	res.send(users[index]);

	
})

app.get('/users/getSingleUser/:index', (req, res) => {

	console.log(req.params);

	let index = parseInt(req.params.index)

	res.send(users[index]);

})







//ITEMS


app.get('/items', (req, res) => {
	res.send(items);
})

app.post('/items', (req, res) => {

	let newItem = {
		name: req.body.name,
		price: req.body.price,
		isActive: req.body.isActive
	}

	items.push(newItem);

	res.send(items);
})

app.put('/items/:index', (req, res) => {

	let index = parseInt(req.params.index);

	items[index].price = req.body.price;

	res.send(items[index]);


})


//ACTIVITY

app.get('/items/getSingleItem/:index', (req, res) => {

	let index = parseInt(req.params.index);
	res.send(items[index]);
})

app.put('/items/archive/:index', (req, res) => {
	
	let index = parseInt(req.params.index);
	items[index].isActive = false;
	res.send(items[index]);
})

app.put('/items/activate/:index', (req, res) => {
	
	let index = parseInt(req.params.index);
	items[index].isActive = true;
	res.send(items[index]);

})


app.listen(port, ()=>console.log(`running at port ${port}`));